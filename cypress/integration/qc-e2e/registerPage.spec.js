Cypress.on("uncaught:exception", (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false;
});

describe("Testing register page", () => {
  before(() => {
    cy.visit("http://new2-wgshub.stagingapps.net/", {
      failOnStatusCode: false,
    });
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce("session_id", "remember_token");
    cy.reload();
  });

  it("register with input all field", () => {
    cy.contains("Register").click();
    cy.get("#register_name").type("Vina Fahira");
    cy.get("#register_lastname").type("Pauziah");
    cy.get("#register_email").type("fahira181@gmail.com");
    cy.get("#register_password").type("Fahira123*");
    cy.get("#register_confirm_password").type("Fahira123*");

    cy.get("#register_company").type("WGS");
    cy.get("#register_address").type(
      "Soekarno-Hatta St No.104, Babakan Ciparay, Bandung City, West Java 40222 Phone Number: 12345678911"
    );
    cy.get("select").select(3).should("have.value", "ID");
    cy.get("#register_phone").type("89123456789");
    cy.get("#register_city").type("Bandung");
    cy.get("#register_submit").click();
    cy.wait(5000);
  });

  it("register without input city field", () => {
    cy.get("#register_name").type("Vina Fahira");
    cy.get("#register_lastname").type("Pauziah");
    cy.get("#register_email").type("fahira181@gmail.com");
    cy.get("#register_password").type("Fahira123*");
    cy.get("#register_confirm_password").type("Fahira123*");
    cy.get("#register_company").type("WGS");
    cy.get("#register_address").type(
      "Soekarno-Hatta St No.104, Babakan Ciparay, Bandung City, West Java 40222 Phone Number: 12345678911"
    );
    cy.get("select").select(3).should("have.value", "ID");
    cy.get("#register_phone").type("89123456789");
    cy.get("#newslatter").uncheck();
    cy.get("#register_submit").click();
  });

  it("require phone number", () => {
    cy.get("#register_name").type("Vina Fahira");
    cy.get("#register_lastname").type("Pauziah");
    cy.get("#register_email").type("fahira181@gmail.com");
    cy.get("#register_password").type("Fahira123*");
    cy.get("#register_confirm_password").type("Fahira123*");
    cy.get("#register_company").type("WGS");
    cy.get("#register_address").type(
      "Soekarno-Hatta St No.104, Babakan Ciparay, Bandung City, West Java 40222 Phone Number: 12345678911"
    );
    cy.get("select").select(3).should("have.value", "ID");
    cy.get("#register_city").type("Bandung");
    cy.get("#newslatter").uncheck();
    cy.get("#register_submit").click();
  });

  it("require company name", () => {
    cy.get("#register_name").type("Vina Fahira");
    cy.get("#register_lastname").type("Pauziah");
    cy.get("#register_email").type("fahira181@gmail.com");
    cy.get("#register_password").type("Fahira123*");
    cy.get("#register_confirm_password").type("Fahira123*");
    cy.get("#register_address").type(
      "Soekarno-Hatta St No.104, Babakan Ciparay, Bandung City, West Java 40222 Phone Number: 12345678911"
    );
    cy.get("select").select(3).should("have.value", "ID");
    cy.get("#register_phone").type("89123456789");
    cy.get("#register_city").type("Bandung");
    cy.get("#newslatter").uncheck();
    cy.get("#register_submit").click();
  });

  it("require confirm password", () => {
    cy.get("#register_name").type("Vina Fahira");
    cy.get("#register_lastname").type("Pauziah");
    cy.get("#register_email").type("fahira181@gmail.com");
    cy.get("#register_password").type("Fahira123*");
    cy.get("#register_company").type("WGS");
    cy.get("#register_address").type(
      "Soekarno-Hatta St No.104, Babakan Ciparay, Bandung City, West Java 40222 Phone Number: 12345678911"
    );
    cy.get("select").select(3).should("have.value", "ID");
    cy.get("#register_phone").type("89123456789");
    cy.get("#register_city").type("Bandung");
    cy.get("#newslatter").uncheck();
    cy.get("#register_submit").click();
  });

  it("require password", () => {
    cy.get("#register_name").type("Vina Fahira");
    cy.get("#register_lastname").type("Pauziah");
    cy.get("#register_email").type("fahira181@gmail.com");
    cy.get("#register_confirm_password").type("Fahira123*");
    cy.get("#register_company").type("WGS");
    cy.get("#register_address").type(
      "Soekarno-Hatta St No.104, Babakan Ciparay, Bandung City, West Java 40222 Phone Number: 12345678911"
    );
    cy.get("select").select(3).should("have.value", "ID");
    cy.get("#register_phone").type("89123456789");
    cy.get("#register_city").type("Bandung");
    cy.get("#newslatter").uncheck();
    cy.get("#register_submit").click();
  });

  it("confirm password doesn't match", () => {
    cy.get("#register_name").type("Vina Fahira");
    cy.get("#register_lastname").type("Pauziah");
    cy.get("#register_email").type("fahira181@gmail.com");
    cy.get("#register_password").type("Fahira123*");
    cy.get("#register_confirm_password").type("Fahira123***");
    cy.get("#register_company").type("WGS");
    cy.get("#register_address").type(
      "Soekarno-Hatta St No.104, Babakan Ciparay, Bandung City, West Java 40222 Phone Number: 12345678911"
    );
    cy.get("select").select(3).should("have.value", "ID");
    cy.get("#register_phone").type("89123456789");
    cy.get("#register_city").type("Bandung");
    cy.get("#newslatter").uncheck();
    cy.get("#register_submit").click();
  });

  it("require email", () => {
    cy.get("#register_name").type("Vina Fahira");
    cy.get("#register_lastname").type("Pauziah");
    cy.get("#register_password").type("Fahira123*");
    cy.get("#register_confirm_password").type("Fahira123***");
    cy.get("#register_company").type("WGS");
    cy.get("#register_address").type(
      "Soekarno-Hatta St No.104, Babakan Ciparay, Bandung City, West Java 40222 Phone Number: 12345678911"
    );
    cy.get("select").select(3).should("have.value", "ID");
    cy.get("#register_phone").type("89123456789");
    cy.get("#register_city").type("Bandung");
    cy.get("#newslatter").uncheck();
    cy.get("#register_submit").click();
  });

  it("require valid email", () => {
    cy.get("#register_name").type("Vina Fahira");
    cy.get("#register_lastname").type("Pauziah");
    cy.get("#register_email").type("fahira181");
    cy.get("#register_password").type("Fahira123*");
    cy.get("#register_confirm_password").type("Fahira123***");
    cy.get("#register_company").type("WGS");
    cy.get("#register_address").type(
      "Soekarno-Hatta St No.104, Babakan Ciparay, Bandung City, West Java 40222 Phone Number: 12345678911"
    );
    cy.get("select").select(3).should("have.value", "ID");
    cy.get("#register_phone").type("89123456789");
    cy.get("#register_city").type("Bandung");
    cy.get("#newslatter").uncheck();
    cy.get("#register_submit").click();
  });

  it("require name", () => {
    cy.get("#register_name").type("Vina Fahira");
    cy.get("#register_lastname").type("Pauziah");
    cy.get("#register_email").type("fahira181@gmail.com");
    cy.get("#register_password").type("Fahira123*");
    cy.get("#register_confirm_password").type("Fahira123***");
    cy.get("#register_company").type("WGS");
    cy.get("#register_address").type(
      "Soekarno-Hatta St No.104, Babakan Ciparay, Bandung City, West Java 40222 Phone Number: 12345678911"
    );
    cy.get("select").select(3).should("have.value", "ID");
    cy.get("#register_phone").type("89123456789");
    cy.get("#register_city").type("Bandung");
    cy.get("#newslatter").uncheck();
    cy.get("#register_submit").click();
  });
});

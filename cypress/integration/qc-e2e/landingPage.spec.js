Cypress.on("uncaught:exception", (err, runnable) => {
  // returning false here prevents Cypress from
  // failing the test
  return false;
});

describe("E2E Testing Web WGSHub", () => {
  before(() => {
    cy.visit("https://new2-wgshub.stagingapps.net/", {
      failOnStatusCode: false,
    });
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce("session_id", "remember_token");
    cy.reload();
  });

  it("login with invalid user", () => {
    cy.contains("Login").click();
    cy.get("#email").type("vina@mail.com");
    cy.get("#password").type("12345");
    cy.get("#login_button").click();
    cy.get(".alert-danger").should(
      "contain",
      "Wrong login credential. Please try again."
    );
  });

  it("login with wrong password", () => {
    cy.get("#email").type("vina.fahira@mail.unpas.ac.id");
    cy.get("#password").type("12345");
    cy.get("#login_button").click();
    cy.get(".alert-danger").should("contain", "Invalid email or password");

    cy.wait(1000);
  });

  it("login with invalid email", () => {
    cy.get("#email").type("test123456789");
    cy.get("#password").type("12345");
    cy.get("#login_button").click();
    cy.get(".alert-danger").should(
      "contain",
      "Wrong login credential. Please try again."
    );
    cy.wait(1000);
  });

  it("login with valid user", () => {
    cy.get("#email").type("vina.fahira@mail.unpas.ac.id");
    cy.get("#password").type("5WYKev7CzCe4cw6");
    cy.get("#login_button").click().wait(10000);
  });
});
